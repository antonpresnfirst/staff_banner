{capture name="mainbox"}

<form action="{""|fn_url}" method="post" name="manage_staff_form" id="manage_staff_form">

{include file="common/pagination.tpl" save_current_page=true save_current_url=true div_id=$smarty.request.content_id}

{assign var="c_url" value=$config.current_url|fn_query_remove:"sort_by":"sort_order"}

{assign var="rev" value=$smarty.request.content_id|default:"pagination_contents"}
{assign var="c_icon" value="<i class=\"exicon-`$search.sort_order_rev`\"></i>"}
{assign var="c_dummy" value="<i class=\"exicon-dummy\"></i>"}

{if $staff}
<table width="100%" class="table table-middle">
<thead>
<tr>
    
    <th width="10%" class=""><a class="cm-ajax" href="{"`$c_url`&sort_by=position&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("position")}{if $search.sort_by == "position"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
	<th width="5%"><span>{__("image")}</span></th>
	<th width="35%"><a class="cm-ajax" href="{"`$c_url`&sort_by=name&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("name")}{if $search.sort_by == "name"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a> </th>
    <th width="15%"><a class="cm-ajax" href="{"`$c_url`&sort_by=function&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("staff_function")}{if $search.sort_by == "function"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a> </th>
    <th width="15%"><a class="cm-ajax" href="{"`$c_url`&sort_by=email&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("email")}{if $search.sort_by == "email"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a> </th>
    <th width="15%"><a class="cm-ajax" href="{"`$c_url`&sort_by=user&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("user")}{if $search.sort_by == "user"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a> </th>
    
</tr>
</thead>
{foreach from=$staff item=staff_member}

<tr class="cm-row">
    <td class="">
        <input type="text" name="staff_data[{$staff_member.staff_id}][position]" size="3" value="{$staff_member.position}" class="input-micro" /></td>
    <td>
        {include file="common/image.tpl" image=$staff_member.photo.detailed image_id=$staff_member.photo.detailed_id image_width=50 href="staff.update?staff_id=`$staff_member.staff_id`"|fn_url}
    </td>
    <td>
        <a class="row-status" href="{"staff.update?staff_id=`$staff_member.staff_id`"|fn_url}">{$staff_member.name|truncate:40 nofilter}</a>        
    </td>
    <td>
        <input type="hidden" name="staff_data[{$staff_member.staff_id}][function]" value="{$staff_member.function}" />
        <a class="row-status" href="{"staff.update?staff_id=`$staff_member.staff_id`"|fn_url}">{$staff_member.function|truncate:40 nofilter}</a>        
    </td>
    <td>
        <input type="hidden" name="staff_data[{$staff_member.staff_id}][email]" value="{$staff_member.email}" />
        <a class="row-status" href="{"staff.update?staff_id=`$staff_member.staff_id`"|fn_url}">{$staff_member.email|truncate:40 nofilter}</a>        
    </td>    
    <td>        
        <a class="row-status" href="{"profiles.update?user_id=`$staff_member.user_id`"|fn_url}">{$staff_member.user|truncate:40 nofilter}</a>        
    </td>
	<td class="nowrap">
        <div class="hidden-tools">
            {capture name="tools_list"}
            	<li>{btn type="list" text=__("edit") href="staff.update?staff_id=`$staff_member.staff_id`"}</li>
				<li>{btn type="list" text=__("delete") class="cm-confirm cm-post" href="staff.delete?staff_id=`$staff_member.staff_id`"}</li>
			{/capture}
            {dropdown content=$smarty.capture.tools_list}
        </div>
    </td>
</tr>
{/foreach}
</table>
{else}
    <p class="no-items">{__("no_data")}</p>
{/if}

{capture name="adv_buttons"}
    {include file="common/tools.tpl" tool_href="staff.add" prefix="top" title=__("add_staff") hide_tools=true icon="icon-plus"}
{/capture}

<div class="clearfix">
    {include file="common/pagination.tpl" div_id=$smarty.request.content_id}
</div>

</form>

{/capture}

{include file="common/mainbox.tpl" title=__("staff") content=$smarty.capture.mainbox title_extra=$smarty.capture.title_extra adv_buttons=$smarty.capture.adv_buttons sidebar=$smarty.capture.sidebar content_id="manage_staff"}