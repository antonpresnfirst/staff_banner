{capture name="mainbox"}

    {capture name="tabsbox"}
        {** /Item menu section **}

        {if $staff_data.staff_id}
            {assign var="id" value=$staff_data.staff_id}
        {else}
            {assign var="id" value=0}
        {/if}
        
        
        <form action="{""|fn_url}" method="post" name="staff_update_form" class="form-horizontal form-edit  cm-disable-empty-files {if ""|fn_check_form_permissions}cm-hide-inputs{/if}" enctype="multipart/form-data" id="staff_update_form_{$id}"> {* staff update form *}
            <input type="hidden" name="result_ids" value="staff_update_form_{$id},content_files"/>
            <input type="hidden" name="fake" value="1" />
            <input type="hidden" class="" name="staff_id" value="{$id}" />

            {** Video description section **}

            <div class="staff-manage" id="content_detailed"> {* content detailed *}

                {** General info section **}
                {include file="common/subheader.tpl" title=__("information") target="#acc_information"}

                <div id="acc_information" class="collapse in">

                    <div class="control-group ">
                        <label for="elm_staff_firstname" class="control-label">{__("firstname")}</label>
                        <div class="controls">                            
                            <input class="input-large" form="staff_update_form_{$id}" type="text" name="staff_data[firstname]" id="elm_staff_firstname" size="55" value="{$staff_data.firstname}" />
                        </div>
                    </div>
                    <div class="control-group ">
                        <label for="elm_staff_lastname" class="control-label">{__("lastname")}</label>
                        <div class="controls">                            
                            <input class="input-large" form="staff_update_form_{$id}" type="text" name="staff_data[lastname]" id="elm_staff_lastname" size="55" value="{$staff_data.lastname}" />
                        </div>
                    </div>
                    <div class="control-group ">
                        <label for="elm_staff_email" class="control-label ">{__("email")}</label>
                        <div class="controls">                            
                            <input class="input-large" form="staff_update_form_{$id}" type="text" name="staff_data[email]" id="elm_staff_email" size="55" value="{$staff_data.email}" />
                        </div>
                    </div>
					<div class="control-group cm-no-hide-input">
                        <label class="control-label" for="elm_staff_function">{__("staff_function")}:</label>
                        <div class="controls">                    
                            <textarea id="elm_staff_function" name="staff_data[function]" cols="55" rows="8" class="cm-wysiwyg input-large">{$staff_data.function}</textarea>
                        </div>
                    </div>
						
                    <div class="control-group ">
                        <label for="elm_staff_position" class="control-label ">{__("position")}</label>
                        <div class="controls">                            
                            <input class="input-large" form="staff_update_form_{$id}" type="text" name="staff_data[position]" id="elm_staff_position" size="55" value="{$staff_data.position}" />
                        </div>
                    </div>
   
					<div class="control-group ">
                        <label for="elm_staff_user_id" class="control-label ">{__("user_id")}</label>
                        <div class="controls">                            
                            <input class="input-small" form="staff_update_form_{$id}" type="text" name="staff_data[user_id]" id="elm_staff_user_id" size="55" value="{$staff_data.user_id}" />
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <label class="control-label">{__("image")}:</label>
                        <div class="controls">
							{assign var="photo_key" value="0"}
							<input type="hidden" name="staff_photo_image_data[{$photo_key}][pair_id]" value="{$staff_data.photo.pair_id}" class="cm-image-field" />
							<input type="hidden" name="staff_photo_image_data[{$photo_key}][type]" value="M" class="cm-image-field" />
							<input type="hidden" name="staff_photo_image_data[{$photo_key}][object_id]" value="{$staff_data.staff_id}" class="cm-image-field" />

							<div class="image">
								{if $staff_data.photo.detailed_id}
									{include file="common/image.tpl" image=$staff_data.photo.detailed image_id=$staff_data.photo.detailed_id image_width=85}
								{else}
									<div class="no-image"><i class="glyph-image" title="{__("no_image")}"></i></div>
								{/if}
							</div>
							
							<div class="image-upload cm-hide-with-inputs">
								{include file="common/fileuploader.tpl" var_name="staff_photo_image_detailed[`$photo_key`]" is_image=true}
							</div>
                            
                        </div>
                    </div>
                                        
                </div>

                <hr>
                
                <!--content_detailed--></div> {* /content detailed *}

          
			{** Form submit section **}
            {capture name="buttons"}
                {include file="common/view_tools.tpl" url="staff.update?staff_id="}

                {if $id}
                    {$view_uri = "staff.view?staff_id=`$id`"|fn_get_preview_url:$staff_data:$auth.user_id}

                    {capture name="tools_list"}
                       <li>{btn type="list" text=__("delete") class="cm-confirm" href="staff.delete?staff_id=`$id`"}</li>
						
                    {/capture}
                    {dropdown content=$smarty.capture.tools_list}
                {/if}
                {include file="buttons/save_cancel.tpl" but_role="submit-link" but_name="dispatch[staff.update]" but_target_form="staff_update_form_`$id`" save=$id}
            {/capture}
            {** /Form submit section **}

		<!--staff_update_form_{$id}--></form> {* /staff update form *}
    {/capture}
    {include file="common/tabsbox.tpl" content=$smarty.capture.tabsbox group_name=$runtime.controller active_tab=$smarty.request.selected_section track=true}

{/capture}

{if $id}
    {capture name="mainbox_title"}
        {"{__('va_editing_staff')}: `$staff_data.name`"|strip_tags}
    {/capture}
{else}
    {capture name="mainbox_title"}
        {__("va_new_staff")}
    {/capture}
{/if}


{include file="common/mainbox.tpl"
    title=$smarty.capture.mainbox_title
    content=$smarty.capture.mainbox
    select_languages=false
    buttons=$smarty.capture.buttons
 
}