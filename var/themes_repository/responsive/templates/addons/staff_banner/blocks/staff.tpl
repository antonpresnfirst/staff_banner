{$obj_prefix = "`$block.block_id`000"}

{if $block.properties.outside_navigation == "Y"}
    <div class="staff-banner-theme ">
        <div class="owl-controls clickable owl-controls-outside" id="owl_outside_nav_{$block.block_id}">
            <div class="owl-buttons">
                <div id="owl_prev_{$obj_prefix}" class="owl-prev"><i class="staff-icon-car-left"></i></div>
                <div id="owl_next_{$obj_prefix}" class="owl-next"><i class="staff-icon-car-right"></i></div>
            </div>
        </div>
    </div>
{/if}

<ul id="scroll_list_{$block.block_id}" class="staff-banner-carousel">
    {foreach from=$staff item="staff_member" name="for_staff_members"}
	<li class="item">
		{include file="common/image.tpl" class="" 
				image_width=$block.properties.thumbnail_width image_height=$block.properties.thumbnail_width 
				images=$staff_member.photo no_ids=true lazy_load=false 
				obj_id="scr_`$block.block_id`000`$staff_member.staff_id`"}
				 
		<h4>{$staff_member.name}</h4>
		{$staff_member.function nofilter	}
		{assign var="email_id" value="staff_email_`$obj_prefix`_`$staff_member.staff_id`"}
		<a class="" id="{$email_id}" href="javascript:void(0);" 
		   onclick="$.ceAjax(
					   'request', 
					   '{"staff.show_email&staff_id=`$staff_member.staff_id`"|fn_url}', 
		   {literal}
				{	
					method: 'POST',				
					cache: true,
					callback: function (data){if (data.text && data.text.length) {{/literal}$('#{$email_id}')[0].outerHTML = data.text;{literal}}}
				}
		   {/literal});">
			{__('text_staff_show_email')}
		</a>
	</li>   
    {/foreach}
</ul>
{include file="common/scroller_init_with_quantity.tpl" items=$staff prev_selector="#owl_prev_`$obj_prefix`" next_selector="#owl_next_`$obj_prefix`"}
