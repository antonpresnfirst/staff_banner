<?php

/* 
 * @author Anton Presnyakov <antonpresn@yandex.ru> 
 * 2015
 */
 
$schema['staff_banner']  = array(
        'templates' => array(
            'addons/staff_banner/blocks/staff.tpl' => array(),
        ),
        'wrappers' => 'blocks/wrappers',
        'content' => array(
            'staff' => array(
                'type' => 'function',
                'function' => array('fn_get_all_staff'),
            )
        ),
        'cache' => array(
            'update_handlers' => array(
                 'staff', 'staff_user', 'users'
            )
        )
    );

return $schema;