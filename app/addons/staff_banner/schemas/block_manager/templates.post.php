<?php

/* 
 * @author Anton Presnyakov <antonpresn@yandex.ru> 
 * 2015
 */

$schema['addons/staff_banner/blocks/staff.tpl'] = array (
//    'fillings' => array('staff_banner.staff'),
//   
    'settings' => array (
        'limit' => array (
            'type' => 'input',
            'default_value' => 20
        ),
        'not_scroll_automatically' => array (
            'type' => 'checkbox',
            'default_value' => 'Y'
        ),
        'speed' =>  array (
            'type' => 'input',
            'default_value' => 400
        ),
        'pause_delay' =>  array (
            'type' => 'input',
            'default_value' => 3
        ),
        'item_quantity' =>  array (
            'type' => 'input',
            'default_value' => 7
        ),
        'outside_navigation' => array (
            'type' => 'checkbox',
            'default_value' => 'Y'
        ),
		
        'thumbnail_width' => array (
            'type' => 'input',
            'default_value' => 180
        ),
	
    ),
);

return $schema;
