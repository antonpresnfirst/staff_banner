<?php

/* 
 * @author Anton Presnyakov <antonpresn@yandex.ru> 
 * 2015
 */

//use Tygh\Embedded;
//use Tygh\Settings;
use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

/**
 * get staff wrapper to select all records with default sorting
 * @return type
 */
function fn_get_all_staff () {
	list ($staff, ) = fn_get_staff (); 
	return $staff;
}

/**
 * Get staff members by params
 * @return array - staff, array - search params
 */
function fn_get_staff ($params = array(), $extra = array()) {
	
	$aliases = array(
		'staff' => 's',
		'staff_users' => 'su',
		'users' => 'u',
	);
			
	$select_fields = array(
		"staff_id" => "$aliases[staff].staff_id",		
		"position" => "$aliases[staff].position",
		"function" => "$aliases[staff].function",
		"user_id" => "$aliases[staff_users].user_id",
		"user_firstname" => "$aliases[users].firstname",
		"user_lastname" => "$aliases[users].lastname",
		"user_email" => "$aliases[users].email",
		"user" => "CONCAT_WS (' ', $aliases[users].firstname, $aliases[users].lastname)"
	);
	
	$b_merge_user_info = (isset($extra['b_merge_user_info']) ? $extra['b_merge_user_info'] : true);
	
	if ($b_merge_user_info) {
		$select_fields = array_merge($select_fields, array(
			"firstname" => "(CASE $aliases[staff].firstname WHEN '' THEN $aliases[users].firstname "
			. "ELSE $aliases[staff].firstname END)",
			"lastname" => "(CASE $aliases[staff].lastname WHEN '' THEN $aliases[users].lastname "
				. "ELSE $aliases[staff].lastname END)",
			"email" => "(CASE $aliases[staff].email WHEN '' THEN $aliases[users].email "
				. "ELSE $aliases[staff].email END)",
			"name" => "CONCAT_WS (' ',"
			. " (CASE $aliases[staff].firstname WHEN '' THEN $aliases[users].firstname ELSE $aliases[staff].firstname END),"
			. " (CASE $aliases[staff].lastname WHEN '' THEN $aliases[users].lastname ELSE $aliases[staff].lastname END))",
		));
	} else {
		$select_fields = array_merge ($select_fields, array(
			"firstname" => "$aliases[staff].firstname",
			"lastname" => "$aliases[staff].lastname",
			"email" => "$aliases[staff].email",
			
			"name" => "CONCAT_WS (' ',"
			. " $aliases[staff].firstname,"
			. " $aliases[staff].lastname)",
		));
	}
	
	$join = "LEFT JOIN ?:staff_users AS $aliases[staff_users]"
			. " ON ($aliases[staff_users].staff_id = $aliases[staff].staff_id)"
			. " LEFT  JOIN ?:users AS $aliases[users]"
			. " ON ($aliases[users].user_id = $aliases[staff_users].user_id)";			
	 
	$conditions = "";
	
	$order = fn_staff_get_order ($params, $select_fields, $extra);
	
	$fields = fn_staff_implode_fields ($select_fields);
	$query = "SELECT $fields FROM ?:staff AS $aliases[staff] $join $conditions $order";
	$staff = db_get_array($query);
	
	if (is_array($staff) && count($staff)) {
		foreach ($staff as &$staff_member) {
			if (!isset($staff_member['staff_id'])) {
				continue;
			}
			
			$staff_member['photo'] = fn_staff_prepare_photo_data ($staff_member['staff_id']);
		}			
	}
	return array($staff, $params);
}

/**
 * Get sorting condition for staff table
 * @param array $params
 * @param string $select_fields - prepared select fields
 * @param string $extra - array of extra parameters
 * @return string
 */
function fn_staff_get_order (&$params, $select_fields, $extra = array()) {
	$order = array ();
	
	if (!empty($params['sort_by']) && isset($select_fields[$params['sort_by']])) {
		$order['BY'] = $select_fields[$params['sort_by']];
	} else {
		$params['sort_by'] = 'position';
		$order['BY'] = $select_fields['position'];
	}
	
	if (!empty($order['BY']) && !empty($params['sort_order'])) {
		$order[strtoupper($params['sort_order'])] = "";	
	}
	
	if (isset($params['sort_order']) && ($params['sort_order'] == 'desc')) {
		$params ['sort_order_rev'] = 'asc';
	} else {
		$params ['sort_order_rev'] = 'desc';
	}
	
	$s_order = "ORDER";
	
	foreach ($order as $k => $v) {
		$s_order .= " ".implode(" ",array($k,$v));
	}
	
	return $s_order;
}


function fn_staff_implode_fields ($select_fields) {
	$s_fields = "";
	$len = count ($select_fields);
	for ($i = 0; $i<$len-1 ; $i++) {
		list ($alias, $select) = each ($select_fields);
		$s_fields .= $select . (!empty($alias) ? " AS $alias" : "") .", ";
	}
	
	list ($alias, $select) = each ($select_fields);
	if (!empty ($select)) {
		$s_fields .= $select . (!empty($alias) ? " AS $alias" : "");
	}
	
	return $s_fields;
}
/**
 * Get data for staff member
 * @param int $staff_id
 * @return array
 */
function fn_get_staff_data ($staff_id) {
	$staff_id = intval($staff_id);
	$staff_alias = 's';
	$staff_users_alias = 'su';
	$users_alias = 'u';
	
	$select_fields = array(
		"$staff_alias.staff_id",
		"$staff_alias.firstname",
		"$staff_alias.lastname",
		"$staff_alias.email",
		"$users_alias.firstname AS user_firstname",
		"$users_alias.lastname AS user_lastname",
		"$users_alias.email AS user_email",
		"$staff_alias.position",
		"$staff_alias.function",
		"$staff_users_alias.user_id",
		"CONCAT_WS (\" \","
		. " $staff_alias.firstname, $staff_alias.lastname) AS name"
	);
	
	$join = "LEFT JOIN ?:staff_users AS $staff_users_alias"
			. " ON ($staff_users_alias.staff_id = $staff_alias.staff_id)"
			. " LEFT JOIN ?:users AS $users_alias"
			. " ON ($users_alias.user_id = $staff_users_alias.user_id)";
	 
	$conditions = "WHERE $staff_alias.staff_id=?i";
	
	$fields = implode(", ",$select_fields);
	$query = "SELECT $fields FROM ?:staff AS $staff_alias $join $conditions";
	
	$data = db_get_row ($query, $staff_id);
	
	$data['photo'] = fn_staff_prepare_photo_data ($staff_id);
	
	return $data;
}

/**
 * Prepare photo data for an image template
 * @param int $staff_id
 * @return array with detailed image info
 */
function fn_staff_prepare_photo_data ($staff_id) {
	
	$photo_data = fn_get_image_pairs($staff_id, MYDEV_STAFF_OBJECT_TYPE, "M", false, true);
	$default_photo_data = array(
		'pair_id' => 0,
		'detailed'=> array(),
		'detailed_id'=> 0
	);
	$photo_data = array_merge($default_photo_data, $photo_data);
	
	return $photo_data;
}

/**
 * Create new staff member record
 * @param array $staff_data
 * @return int/boolean - staff_id or false on error
 */
function fn_create_staff ($staff_data) {
	
	$staff_id = false;
	
	$data = array ();
	
	if (isset($staff_data['user_id'])) {		 
		$user_id = fn_staff_check_user_id ($staff_data['user_id']);
	}
	
	if (empty($staff_data['firstname']) && !empty($user_id)) {
		$data['firstname'] = "";
	} else if (empty($staff_data['firstname'])) {
		$data['firstname'] = null;
	} else if (!empty($staff_data['firstname'])) {
		$data['firstname'] = $staff_data['firstname'];
	}
	
	if (isset($data['firstname'])) {
		if (empty($staff_data['lastname'])) {
			$data['lastname'] = "";
		}	
		
		if (empty($staff_data['email'])) {
			$data['email'] = "";
		}
		
		if (empty($staff_data['function'])) {
			$data['function'] = "";
		}
		
		if (empty($staff_data['position'])) {
			$data['position'] = 100;
		}
		
		$query = "INSERT INTO ?:staff ?e";
		$staff_id = db_query ($query, $data);
		
		if (empty($staff_id)) {
			$staff_id = false;
		}
		
		if (($staff_id !== false) && !empty($user_id)) {
			
			$user_data = array (
				'staff_id' => $staff_id,
				'user_id' => $user_id
			);
			
			$query_users = "REPLACE INTO ?:staff_users SET ?u";
			$arow_users = db_query ($query_users, $user_data);
			
			if ($arow_users === false) {
				fn_set_notification('E', __('error'), __('object_not_found', array('[object]' => __('staff_user'))),'','404');
			}
		}
		
		if ($staff_id !== false) {
			fn_update_staff_photo ($staff_id);
		}
	}
	
	return $staff_id;	
}

/**
 * Update or create staff member
 * @param int $staff_id
 * @param array $staff_data
 * @return int/boolean - staff_id or false on error
 */
function fn_update_staff ($staff_id, $staff_data) {
	
	if (empty($staff_id)) {
		$result = fn_create_staff ($staff_data);
	} else {
		
		$data = array();
		
		if (isset($staff_data['firstname'])) {
			$data['firstname'] = $staff_data['firstname'];
		}
		
		if (isset($staff_data['lastname'])) {
			$data['lastname'] = $staff_data['lastname'];
		}
		
		if (isset($staff_data['position'])) {
			$data['position'] = $staff_data['position'];
		}
		
		if (isset($staff_data['email'])) {
			$data['email'] = $staff_data['email'];
		}
		
		if (isset($staff_data['function'])) {
			$data['function'] = $staff_data['function'];
		}
		
		$query = "UPDATE ?:staff SET ?u WHERE staff_id = ?i";
		$arow = db_query ($query, $data, $staff_id);
		
		if ($arow === false) {
			fn_set_notification('E', __('error'), __('object_not_found', array('[object]' => __('staff_member'))),'','404');
			$staff_id = false;
		} else {
			
			if (isset($staff_data['user_id']) 
				&& ($user_id = fn_staff_check_user_id($staff_data['user_id']))) {
				
				$data_users = array (
					'user_id' => $user_id,
					'staff_id' => $staff_id
				);
				$query_users = "REPLACE INTO ?:staff_users SET ?u";
				$arow_users = db_query ($query_users, $data_users);
				
				if ($arow_users === false) {
					fn_set_notification('E', __('error'), __('object_not_found', array('[object]' => __('staff_user'))),'','404');
				}
			} else if (!empty($staff_data['user_id']) && !$user_id) {
				fn_set_notification('E', __('error'), __('object_not_found', array('[object]' => __('staff_user'))),'','404');
			}
			
			fn_update_staff_photo ($staff_id);			
		}
		
		$result = $staff_id;
	}
	
	return $result;
}

function fn_staff_check_user_id($user_id) {
	$user_id = intval($user_id);
	$b_is_exists = db_get_field ("SELECT (CASE user_id "
			. " WHEN NULL"
			. " THEN FALSE"
			. " ELSE TRUE END)"
			. " FROM ?:users"
			. " WHERE user_id=?i", $user_id);
	
	if (!$b_is_exists) {
		$user_id = $b_is_exists;
	}
	
	return $user_id;	
}

/**
 * Update photo attachment to staff member
 * @param int $staff_id
 * @return array - pair of photo ids (in fact we use only one - image_id without thumbnail)
 */
function fn_update_staff_photo ($staff_id) {	
	
	$staff_photo_ids = array(
		0 => $staff_id
	);
	
	$photo_pairs = fn_attach_image_pairs("staff_photo", MYDEV_STAFF_OBJECT_TYPE, $staff_id, CART_LANGUAGE, $staff_photo_ids);
	
	return $photo_pairs;
}

/**
 * Deletes staff member
 * @param int $staff_id
 * @return boolean - result of deletion
 */
function fn_delete_staff ($staff_id) {
	$staff_id = intval ($staff_id);
	
	$result = db_query("DELETE FROM ?:staff WHERE staff_id=?i", $staff_id);
	
	if ($result) {
		db_query("DELETE FROM ?:staff_users WHERE staff_id=?i", $staff_id);
		
		fn_delete_image_pairs($staff_id, MYDEV_STAFF_OBJECT_TYPE);
	}
	
	return $result;
}

