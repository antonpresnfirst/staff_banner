<?php

/* 
 * @author Anton Presnyakov <antonpresn@yandex.ru> 
 * 2015
 */

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

$_REQUEST['staff_id'] = empty($_REQUEST['staff_id']) ? 0 : $_REQUEST['staff_id'];

if ($_SERVER['REQUEST_METHOD']=='POST') {
	
	 // Define trusted variables that shouldn't be stripped
    fn_trusted_vars (
        'staff_data'
	);
	
	//	
	// Create/update staff_member
	//	
	if ($mode == 'update') {
		if (!empty($_REQUEST['staff_data']['firstname']) || !empty($_REQUEST['staff_data']['user_id'])) {
			$staff_id = fn_update_staff ($_REQUEST['staff_id'], $_REQUEST['staff_data']);
			
			if ($staff_id === false) {
				 // Some error occured
                fn_save_post_data('staff_data');
			}
		} else {
			fn_set_notification('E', __('error'), __('error_staff_firstname'),'','404');
		}
		
		return array(CONTROLLER_STATUS_REDIRECT, !empty($_REQUEST['staff_id']) ? 'staff.update?staff_id=' . $_REQUEST['staff_id'] : 'staff.add');
	}
}

//
// Add new staff member page
//
if ($mode == 'add') {
	$staff_data = fn_restore_post_data('staff_data');
    Tygh::$app['view']->assign('staff_data', $staff_data);

//
// Update staff member page
//	
} else if ($mode == 'update') {
	$staff_data = fn_get_staff_data($_REQUEST['staff_id']);
	
	if (empty($staff_data)) {
        return array(CONTROLLER_STATUS_NO_PAGE);
    }
	
	Tygh::$app['view']->assign('staff_data', $staff_data);

//
// Manage all staff page
//	
} else if ($mode == 'manage') {

	$search = $_REQUEST;
	list ($staff, $search) = fn_get_staff ($search);
	
	Tygh::$app['view']->assign('staff', $staff);
	Tygh::$app['view']->assign('search', $search);
	
//	
// Delete staff member page
//	
} else if ($mode == 'delete') {
	
	if (!empty($_REQUEST['staff_id'])) {
		$result = fn_delete_staff ($_REQUEST['staff_id']);
		
		if ($result) {
			fn_set_notification('N', __('notice'), __('text_staff_has_been_deleted'));
			return array(CONTROLLER_STATUS_REDIRECT, 'staff.manage');
		} else {
			return array(CONTROLLER_STATUS_REDIRECT, 'staff.update?staff_id=' . $_REQUEST['staff_id']);
		}
	}
}