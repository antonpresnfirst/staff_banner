<?php

/* 
 * @author Anton Presnyakov <antonpresn@yandex.ru> 
 * 2015
 */

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == "POST") {
	if ($mode == 'show_email') {
		if (isset($_REQUEST['staff_id'])) {
			$staff_data = fn_get_staff_data($_REQUEST['staff_id']);
			
			$email = (!empty($staff_data['email']) ? $staff_data['email'] 
					: (!empty($staff_data['user_email']) ? $staff_data['user_email'] : ""));
			Tygh::$app['view']->assign('email', $email);
		}
		
		if (empty ($email)) {
			fn_set_notification('W', __('error'), __('object_not_found', array('[object]' => __('email'))),'','404');
		}
	}
}
	